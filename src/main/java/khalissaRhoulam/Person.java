package khalissaRhoulam;

import java.util.GregorianCalendar;
import java.text.Normalizer;

public class Person {
    private String lastName;
    private String firstName;
    private GregorianCalendar birthDate;
    private String eMail;
    private String favoriteColor;

    public Person() {
    }

    //GETTERS

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public String getLogin() {
        String Login = buildLogin();
        Login = stripAccents(Login);
        Login = removeSpecialCharacter(Login);
        return Login;
    }

    public String getEmail() {
        return eMail;
    }

    public String getFavoriteColor() {
        return favoriteColor;
    }

    //SETTERS

    public void setLastName(String lastName) {
        this.lastName = lastName.toUpperCase();
    }

    public void setFirstName(String firstName) {
        String[] parts = firstName.split("-");
        this.firstName = "";
        if (!firstName.equals("")) {
            for (int i=0; i< parts.length; i++) {
                this.firstName += parts[i].substring(0, 1).toUpperCase() +
                        parts[i].substring(1).toLowerCase();
            }
        }
    }

    public void setDdn(int y, int m, int d) {
        this.birthDate = new GregorianCalendar(y, m, d);
        GregorianCalendar now = new GregorianCalendar();
        if (this.birthDate.compareTo(now)>0) {
            throw new IllegalArgumentException("Erreur: date de naissance dans le futur");
        }
    }

    public void setEmail(String e) {
        checkSize(e);
        checkAt(e);
        this.eMail = e;
    }

    public void setFavoriteColor(String favoriteColor) {
        this.favoriteColor = favoriteColor;
    }

    //METHODS

    /**
     * @method buildLogin
     * Create Login with firstName and lastName
     * @return Login
     */
    public String buildLogin() {
        if (this.getFirstName().equals("") && this.getLastName().equals("")) {
            throw new IllegalArgumentException("Last name and first name empty");
        }
        String s = this.getFirstName().substring(0, 1).toLowerCase();
        for (int i = 1; i<this.getFirstName().length(); i++) {
            if (this.getFirstName().substring(i,i+1).equals(this.getFirstName().substring(i,i+1).toUpperCase())) {
                s += this.getFirstName().substring(i,i+1).toLowerCase();
            }
        }
        s += this.getLastName().toLowerCase();
        return s;
    }

    /**
     * @method stripAccents
     * convert accent char into normal char
     * @return converted string
     */
    public String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    /**
     * @method removeSpecialCharacter
     * remove anything that is not a letter
     * @return converted string
     */
    public String removeSpecialCharacter(String s) {
        s = s.replaceAll("[^a-zA-Z0-9]", "");
        s = s.replaceAll(" ", "");
        return s;
    }

    public void checkSize(String e) {
        if (e.length()<5) {
            throw new IllegalArgumentException();
        }
    }

    public void checkAt(String e) {
        int a = 0;
        for (int i=0; i<e.length(); i++) {
            if (e.substring(i,i+1).equals("@")) {
                a = 1;
            }
        }
        if (a == 0) {
            throw new IllegalArgumentException();
        }
    }
}
