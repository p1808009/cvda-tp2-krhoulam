package khalissaRhoulam;

import org.junit.jupiter.api.Test;

import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    void testSetLastName() {
        String a = "dupont";
        String b = "DUPONT";
        Person pers1 = new Person();

        pers1.setLastName(a);

        assertEquals(b,pers1.getLastName());
    }

    @Test
    void testSetFirstName() {
        String a = "Jean-pierre";
        String b = "JeanPierre";
        Person pers1 = new Person();

        pers1.setFirstName(a);

        assertEquals(b,pers1.getFirstName());
    }

    @Test
    void testSetBirthDate() {
        GregorianCalendar a = new GregorianCalendar(2002, 2, 6);
        Person pers1 = new Person();

        pers1.setDdn(2002,2,6);

        assertEquals(a,pers1.getBirthDate());
    }

    @Test
    void testSetLastNameEmpty() {
        Person pers1 = new Person();
        pers1.setLastName("");
        assertEquals("",pers1.getLastName());
    }

    @Test
    void testSetFirstNameEmpty() {
        Person pers1 = new Person();
        pers1.setFirstName("");
        assertEquals("",pers1.getFirstName());
    }

    @Test
    void testSetBirthDateFuture() {

        assertThrows(IllegalArgumentException.class, () -> {
            Person pers1 = new Person();
            pers1.setDdn(2053,8,21);
        });
    }

    @Test
    void testGetLogin() {
        Person pers1 = new Person();
        pers1.setLastName("DUPONT");
        pers1.setFirstName("Jean");
        assertEquals("jdupont",pers1.getLogin());
    }

    @Test
    void testGetLoginAccents() {
        Person pers1 = new Person();
        pers1.setLastName("POINCARRÉ");
        pers1.setFirstName("Raymond");
        assertEquals("rpoincarre",pers1.getLogin());
    }

    @Test
    void testGetLoginSpecialChar() {
        Person pers1 = new Person();
        pers1.setLastName("d’ESTIENNE d’ORVES");
        pers1.setFirstName("Jean-François");
        assertEquals("jfdestiennedorves",pers1.getLogin());
    }

    @Test
    void testGetLoginEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            Person pers1 = new Person();
            pers1.setLastName("");
            pers1.setFirstName("");
            pers1.getLogin();
        });
    }

    @Test
    void testSetEmail() {
        String a = "jean.dupont@gmail.com";
        Person pers1 = new Person();
        pers1.setEmail(a);
        assertEquals(a,pers1.getEmail());
    }

    @Test
    void testSetEmailSize() {
        assertThrows(IllegalArgumentException.class, () -> {
            String a = "j@.f";
            Person pers1 = new Person();
            pers1.setEmail(a);
        });
    }

    @Test
    void testSetEmailAt() {
        assertThrows(IllegalArgumentException.class, () -> {
            String a = "jean.dupontgmail.com";
            Person pers1 = new Person();
            pers1.setEmail(a);
        });
    }
}