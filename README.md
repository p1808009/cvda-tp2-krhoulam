# cvda-tp2-krhoulam

Kanboard : https://g4s22021.bavori.tech/?controller=BoardViewController&action=show&project_id=35&search=status%3Aopen

## EXERCICE 1 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

## EXERCICE 2 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

Question 3 : Pour modifier les colonnes du projet, il faut se rendre dans "configurer ce projet" dans les paramètres en haut à gauche, puis dans "colonnes" et enfin cliquer sur la molette à coté du nom des colonnes et sélectionner "modifier". Par défault, il n'y a pas de limite au nombre de tâches par colonnes (indiqué par le signe infini).

Question 4 : Pour modifier et ajouter des swimlanes, dans "configurer ce projet" se rendre dans "swimlanes" et cliquer sur la molette à coté du nom des swimlanes pour modifier ou cliquer sur "+ Ajouter une nouvelle swimlane" pour en ajouter une.

## EXERCICE 3 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

## EXERCICE 4 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

Question 6 : Le nom de la classe test générée est PersonneTest.

Question 7 : Pour tester les cas nominaux, j'utilise la fonction jUnit ```assertEquals()``` afin de comparer la valeur retournée par la méthode à celle attendue si tout se passe bien.

Question 9 : La fonction jUnit utilisée pour tester le renvoi d'un IllegalArgumentException est la suivante : 
```
assertThrows(IllegalArgumentException.class, () -> {
//cas générant un IllegalArgumentException
});
```
Le test s'effectue sans problèmes si un IllegalArgumentException est bel et bien renvoyé.

## EXERCICE 5 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

Question 1 : Pour la rédaction des méthodes stripAccents et removeSpecialCharater, j'ai emprunté du code sur les sites suivants :

https://stackoverflow.com/questions/15190656/easy-way-to-remove-accents-from-a-unicode-string

https://www.javatpoint.com/how-to-remove-special-characters-from-string-in-java

Question 2 : J'ai choisi comme exception IllegalArgumentException afin d'indiquer que le problème vient des arguments des méthodes getNom et getPrénom, en plus d'un message pour indiquer que c'est le fait que les deux soient vides qui pose problème.

## EXERCICE 6 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

Application des règles de bonne pratique :
- les principes SOLID : Découpage des méthodes pour faire en sorte qu'elles aient qu'un rôle à jouer (facilite le débugage).
- fixation de l'indentation pour faciliter la lecture : choix du type K&R.
- Organisation du code : regroupe setters, getters et nouvelles méthodes ensemble pour faciliter la recherche.
- Renommage des variables et des méthodes pour fixer aux normes
- Exception placé en début de code dans des méthodes qui les concernent.

## EXERCICE 7 ![](https://img.shields.io/badge/complété%20à-100%25-brightgreen%20?style=plastic&logo=gitlab)

La javadoc est généré sous forme de dossier dans le répertoire que l'on souhaite.